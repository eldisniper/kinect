﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using UnityEngine.UI;

public class BodySourceView : MonoBehaviour 
{
    public Material BoneMaterial;
    public Button btnStart;
    public Button btnSettings;
    public Button btnHelp;
    public GameObject BodySourceManager;
    public GameObject Boat;
    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodySourceManager _BodyManager;
    public Canvas NotTracked;
    public Canvas DetectDisp;
    public Canvas Game;
    public Canvas Settings;
    public Canvas Menu;
    public Canvas Help;
    public Canvas paused;
    public GameObject Halo;
    public GameObject bodysetting;
    bool _exist = false;
    public RawImage cursur;
    public Button start;
    public Sprite epee;
    public RawImage cursurSet;
    BoatController boatC;
    private Kinect.Body Now;
    float zepee;
	public static bool pause = false;

    //sounds
   public AudioSource gameSound;
    public AudioSource clickSound;
    public AudioSource effectSound;



    private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },
        
        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },
        
        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },
        
        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },
        
        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };
    void Start()
    {
        boatC = Boat.GetComponent<BoatController>();
        zepee = cursur.transform.position.z;
        pause = false;

    }
    void Update()
    {

        if (BodySourceManager == null)
        {
            return;
        }

        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null)
        {

            return;

        }

        Kinect.Body[] data = _BodyManager.GetData();
        if (data == null)
        {
            return;
        }

        List<ulong> trackedIds = new List<ulong>();

        foreach (var body in data)
        {

            if (body == null)
            {
                continue;

            }

            if (body.IsTracked)
            {
                trackedIds.Add(body.TrackingId);
            }

        }

        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);

        // First delete untracked bodies
        foreach (ulong trackingId in knownIds)
        {
            if (!trackedIds.Contains(trackingId))
            {
                Destroy(_Bodies[trackingId]);
                // Corps a disparu
                _exist = false;
                _Bodies.Remove(trackingId);
            }
        }
       if (_exist)
        {
            RefreshBodyObject(Now, _Bodies[Now.TrackingId]);
            if (Now.IsTracked)
            {

                if (PlayerPrefs.GetInt("hand") == 1)
                {
                    if (Now.HandRightState == Kinect.HandState.Closed)
                    {
                       
                            if (Settings.enabled)
                            {
                                cursorSetting.Change(false);
                            }
                            switch (collisionDetect.ButtonClicked)
                            {
                                case 1:
                                    if (Menu.enabled)
                                    {
                                        clickSound.Play(); //Click sound 
                                        MenuToGame.MoveIt();
                                        gameSound.Play();   // Play sound game
                                    }

                                    break;
                                case 2:
                                    Menu.enabled = false;
                                    Help.enabled = true;
                                    break;
                                case 3:
                                    //Click sound 
                                    Menu.enabled = false;
                                    bodysetting.SetActive(true);
                                    Settings.enabled = true;
                                    break;
                                default:
                                    break;
                            
                        }

                    }
                }
                else
                {
                    if (Now.HandLeftState == Kinect.HandState.Closed)
                    {
                       

                            if (Settings.enabled)
                            {
                                cursorSetting.Change(false);
                            }
                            switch (collisionDetect.ButtonClicked)
                            {
                                case 1:
                                    MenuToGame.MoveIt();
                                    gameSound.Play();   // Play sound game
                                    break;
                                case 2:
                                    Menu.enabled = false;
                                    Help.enabled = true;
                                    cursurSet.transform.position = new Vector3(0, 0, 0);
                                    cursur.transform.position = new Vector3(0, 0, 0);
                                    break;
                                case 3:
                                    Menu.enabled = false;
                                    bodysetting.SetActive(true);
                                    Settings.enabled = true;
                                    break;
                                default:
                                    break;
                            
                        }

                    }
                }


               
                if (PlayerPrefs.GetInt("hand") == 1)
                {
                    if (Now.HandRightState == Kinect.HandState.Open)
                    {
                        if (Settings.enabled)
                        {
                            cursorSetting.Change(true);
                        }
                    }
                }
                else
                {
                    if (Now.HandLeftState == Kinect.HandState.Open)
                    {
                        if (Settings.enabled)
                        {
                            cursorSetting.Change(true);
                        }
                    }
                }
            }
            }
            else
        {
            foreach (var body in data)
            {
                if (body == null)
                {

                    continue;
                }
                else
                {
                    Now = body;
                }



                if (body.IsTracked)
                {

                    
					if(PlayerPrefs.GetInt ("hand") == 1)
                    if (body.HandRightState == Kinect.HandState.Closed)
                    {
                        switch (collisionDetect.ButtonClicked)
                        {
                            case 1:
                               
                                MenuToGame.MoveIt();
                                gameSound.Play();
                                break;
                            default:
                                break;
                        }
                    }
					else
					{
						if (body.HandLeftState == Kinect.HandState.Closed)
						{
							switch (collisionDetect.ButtonClicked)
							{
							case 1:
								 
								MenuToGame.MoveIt();
								gameSound.Play();
								break;
							default:
								break;
							}
						}
					}



                    //Kinect.FrameEdges.Left



                    if (!_Bodies.ContainsKey(body.TrackingId))
                    {
                        _Bodies[body.TrackingId] = CreateBodyObject(body.TrackingId);
                        _exist = true;
                        if (MenuToGame.started)
                            NotTracked.enabled = false;
                        if (_exist)
                        {
                            DetectDisp.enabled = false;
                        }
                    }

                    RefreshBodyObject(body, _Bodies[body.TrackingId]);
                    break;
                }


            }
            if (!_exist)
            {
                DetectDisp.enabled = true;
                if(MenuToGame.started)
                {
                    pause = true;
                    Time.timeScale = 0;
                    gameSound.Pause();
                    effectSound.Pause();
                }
            }
            else
            {
                if (MenuToGame.started && !paused.enabled)
                {
                    pause = false;
                    Time.timeScale = 1;
                    gameSound.Play();
                    effectSound.Play();
                }
             
            }
        }
            

    }
    
    private GameObject CreateBodyObject(ulong id)
    {
        GameObject body = new GameObject("Body:" + id);

	
        body.transform.position = NotTracked.transform.position;
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
            
         
            LineRenderer lr = jointObj.AddComponent<LineRenderer>();
            lr.SetVertexCount(2);
            lr.material = BoneMaterial;
            lr.SetWidth(0.05f, 0.05f);
            
            jointObj.transform.localScale = new Vector3(0f, 0f, 0f);
            jointObj.name = jt.ToString();
            jointObj.transform.parent = body.transform;
            //jointObj.transform.position = Boat.transform.position;
            
        }
        
        return body;
    }
    bool fermer = true;
	Kinect.JointType joins = Kinect.JointType.ThumbRight;
    private void RefreshBodyObject(Kinect.Body body, GameObject bodyObject)
    {


        if (body.HandLeftState == Kinect.HandState.Closed && body.HandRightState == Kinect.HandState.Closed && Help.enabled)
        {
            cursurSet.transform.position = new Vector3(0, 0, 0);
            cursur.transform.position = new Vector3(0, 0, 0);
            Help.enabled = false;
            Menu.enabled = true;
        }

        if (body.HandLeftState == Kinect.HandState.Closed && body.HandRightState == Kinect.HandState.Closed && Game.enabled)
        {
            if(fermer)
            {
                fermer = false;
                if (pause)
                {
                    paused.enabled = false;
                    pause = false;
                    Time.timeScale = 1;
                    gameSound.Play();
                    effectSound.Play();

                }
                else
                {
                    pause = true;
                    Time.timeScale = 0;
                    gameSound.Pause();
                    effectSound.Pause();
                    paused.enabled = true;
                }
            }
            

        }


        if (body.HandLeftState == Kinect.HandState.Open && body.HandRightState == Kinect.HandState.Open)
        {

            fermer = true;

        }

        if (!pause) {

		

		if(PlayerPrefs.GetInt ("hand") == 0)
			joins = Kinect.JointType.ThumbLeft;
		else
			joins =  Kinect.JointType.ThumbRight;

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= joins; jt++)
        {
            Kinect.Joint sourceJoint = body.Joints[jt];
            Kinect.Joint? targetJoint = null;
            
            if(_BoneMap.ContainsKey(jt))
            {
                targetJoint = body.Joints[_BoneMap[jt]];
            }

            
            Transform jointObj = bodyObject.transform.FindChild(jt.ToString());
            jointObj.localPosition = GetVector3FromJoint(sourceJoint);
            Boat.transform.position = new Vector3(jointObj.localPosition.x, 0, Boat.transform.position.z);
            LineRenderer lr = jointObj.GetComponent<LineRenderer>();
            if(targetJoint.HasValue)
            {
                lr.SetPosition(0, jointObj.localPosition);
                lr.SetPosition(1, GetVector3FromJoint(targetJoint.Value));
                lr.SetColors(GetColorForState (sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
            }
            else
            {
                lr.enabled = false;
            }
                
            if (Menu.enabled)
            {
                Vector3 p;
				if (PlayerPrefs.GetInt("hand") == 1)
				p = bodyObject.transform.Find("HandRight").position;
				else
				p = bodyObject.transform.Find("HandLeft").position;
              
                p.y -= 4;
                p.z = zepee;
                    
                cursur.transform.position = p;
            }
            else
                {
                    if (Settings.enabled)
                    {
                        Vector3 p;
                        if (PlayerPrefs.GetInt("hand") == 1)
                            p = bodyObject.transform.Find("HandRight").position;
                        else
                            p = bodyObject.transform.Find("HandLeft").position;

                        p.y -= 4;
                        p.z = zepee;
                        cursurSet.transform.position = p;
                    }
                   
                }
           



			}}
    }
    
    private static Color GetColorForState(Kinect.TrackingState state)
    {
        switch (state)
        {
        case Kinect.TrackingState.Tracked:
            return Color.green;

        case Kinect.TrackingState.Inferred:
            return Color.red;

        default:
            return Color.black;
        }
    }
    
    private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
    {
        return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
    }
}
