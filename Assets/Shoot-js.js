﻿
public var obu : GameObject;
public var bateau : GameObject;
public var addtest : Vector3;
public var Game : Canvas;
public var Sound: AudioSource;
function Start () {
	InvokeRepeating("Shoot",1f, Random.Range(5f,15f));  
}

function Update () {

	     
 }
function Shoot()
{
 
 if (Game.enabled &&  Time.timeScale == 1)
 {
    var ball: GameObject = Instantiate(obu, transform.position, transform.rotation);
    var rb = ball.GetComponent.<Rigidbody>();
    rb.velocity = BallisticVel(bateau.transform);
    Sound.Play();
    Destroy(ball, 15);
  }
}
function BallisticVel(target: Transform): Vector3 {

 var _temp : Vector3;
 _temp = target.position;
 _temp.x += Random.Range(0,12);
 var dir = _temp	 - transform.position + addtest; // get target direction
 var h = dir.y;  // get height difference
 dir.y = 0;  // retain only the horizontal direction
 var dist = dir.magnitude ;  // get horizontal distance
 dir.y = dist;  // set elevation to 45 degrees
 dist += h;  // correct for different heights
 var vel = Mathf.Sqrt(dist * Physics.gravity.magnitude);
 return vel * dir.normalized;  // returns Vector3 velocity
}