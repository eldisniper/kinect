﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class cursorSetting : MonoBehaviour
{

    public Texture closeHand;
    public Texture openHand;
    public RawImage daySelect;
    public RawImage nightSelect;
    public RawImage leftSelect;
    public RawImage rightSelect;
    public Canvas Setting;
    public Canvas Menu;
    public Canvas Game;
    public GameObject body;
    public Material day;
    public Material night;
    public Slider music;
    public Slider effect;
    
    //public static 
    static GameObject cursor;
    public static bool open = true;
    static bool change = false;
    static Vector3 handclosepos;

    // Use this for initialization
    void Start()
    {
        change = false;
        handclosepos = new Vector3();
        cursor = gameObject;
        PlayerPrefs.Save();
        ChangeHand();
        ChangeSky();

    }


    // Update is called once per frame
    void Update()
    {
        if (change)
        {
            if (open)
                OpenHand();
            else
                CloseHand();
            change = false;
        }


    }
    public static void Change(bool _open)
    {
        if (_open != open)
        {
            open = _open;
            change = true;
            handclosepos = cursor.transform.position;
        }

    }
    void OpenHand()
    {
        cursor.GetComponent<RawImage>().texture = openHand;
    }
    void CloseHand()
    {
        cursor.GetComponent<RawImage>().texture = closeHand;
    }
    void OnTriggerStay(Collider other)
    {
        if(!open)
        switch (other.gameObject.name)
        {
            case "night":
                PlayerPrefs.SetInt("sky", 0);
                ChangeSky();
                break;
            case "day":
                PlayerPrefs.SetInt("sky", 1);
                ChangeSky();
                break;
            case "left":
                Debug.Log("left");
                PlayerPrefs.SetInt("hand", 0);
                ChangeHand();
                break;
            case "right":
                Debug.Log("right");
                PlayerPrefs.SetInt("hand", 1);
                ChangeHand();
                break;
            case "btnBack":
                  
                    
                        Setting.enabled = false;
                        body.SetActive(false);
                        Menu.enabled = true;
                        open = true;
                    
                
                break;
            case "effecthndl":
                Debug.Log((gameObject.transform.position.x - handclosepos.x) / 100);
                music.value = music.value + (gameObject.transform.position.x - handclosepos.x) / 100;
                break;

        }
    }

    void ChangeHand()
    {
        if (PlayerPrefs.GetInt("hand", 1) == 1)
        {
            rightSelect.enabled = true;
            leftSelect.enabled = false;

        }
        else
        {
            leftSelect.enabled = true;
            rightSelect.enabled = false;
        }
    }


    void ChangeSky()
    {
        if (PlayerPrefs.GetInt("sky", 1) == 1)
        {
            (Camera.main.GetComponent<Skybox>()).material = day;
            daySelect.enabled = true;
            nightSelect.enabled = false;
        }
        else
        {
            (Camera.main.GetComponent<Skybox>()).material = night;
            nightSelect.enabled = true;
            daySelect.enabled = false;
        }
    }
}
