﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class collisionDetect : MonoBehaviour {


    public Button btnStart;
    public Button btnSettings;
    public Button btnHelp;
	public List<Texture> srites;

    public static int ButtonClicked = 0;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    GameObject SelectedBtn;
   void OnTriggerEnter(Collider other)
    {
        (gameObject.GetComponent<RawImage>()).enabled = false;
		SelectedBtn = other.gameObject;
        
        switch (other.gameObject.name)
        {
            case "btnStart":
               
                (GetComponent("Halo") as Behaviour).enabled = false;
                (btnStart.GetComponent<RawImage>()).texture = srites[0];
                ButtonClicked = 1;
                break;
            case "btnHelp":
                (GetComponent("Halo") as Behaviour).enabled = false;
                (btnHelp.GetComponent<RawImage>()).texture = srites[1];
                ButtonClicked = 2;
                break;
            case "btnSettings":
                (GetComponent("Halo") as Behaviour).enabled = false;
                (btnSettings.GetComponent<RawImage>()).texture = srites[2];
                ButtonClicked = 3;
                break;
        } 
        
      
        //Debug.Log(other.gameObject.name);
    }

    void OnTriggerExit(Collider other)
    {
        (gameObject.GetComponent<RawImage>()).enabled = true;
        ButtonClicked = 0;
        switch (SelectedBtn.name)
        {
            case "btnStart":
                (GetComponent("Halo") as Behaviour).enabled = true;
                (btnStart.GetComponent<RawImage>()).texture = srites[3];
                break;
            case "btnHelp":
                (GetComponent("Halo") as Behaviour).enabled = true;
                (btnHelp.GetComponent<RawImage>()).texture = srites[4];
                break;
            case "btnSettings":
                (GetComponent("Halo") as Behaviour).enabled = true;
                (btnSettings.GetComponent<RawImage>()).texture = srites[5];
                break;
        }
    }



}
