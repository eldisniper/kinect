﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BoatController : MonoBehaviour {

	bool moving;
	int direction;
	Vector3 position;
	float Xpos;
	float Ypos;
	float Zpos;
	public float speedx;
	public float speedz;
    public static Vector3 positionBoat;
	public GameObject explosionPrefab;
	public GameObject flare;
	FlareSystem thisFlareSystem ;
	public RectTransform life;
	public RectTransform time;
    public RectTransform lifecnt;
    public RectTransform timecnt;
    public GameObject cursur;
    public Image timeImage;
	public AudioSource boom;
	float decreaselife ;
	// Use this for initialization
	void Start () {
		decreaselife = life.rect.height / 10;
        life.offsetMax = lifecnt.offsetMax;
        time.offsetMax = timecnt.offsetMax;

        thisFlareSystem = new FlareSystem ();
		thisFlareSystem.name = flare.name;
		thisFlareSystem.particleObject = Instantiate (flare, Vector3.zero, Quaternion.identity) as GameObject;
		thisFlareSystem.particleSystems = thisFlareSystem.particleObject.GetComponentsInChildren<ParticleSystem>();

			
		moving = false;
		transform.Translate (0, 0, 1);
		Xpos = gameObject.transform.position.x;
		Ypos = gameObject.transform.position.y;
		Zpos = gameObject.transform.position.z;
		StartCoroutine(timedecrease()); 

	}

	IEnumerator timedecrease()
	{
		
		while(time.offsetMax.y * -1  <timecnt.rect.height)
		{
            if (!BodySourceView.pause)
            {
                if (time.offsetMax.y * -1 >= time.rect.height / 2 && timeImage.color != new Color(255, 140, 0, 188))
                {
                    timeImage.color = new Color(255, 140, 0, 188);
                }
                if (time.offsetMax.y * -1 >= time.rect.height * 3 / 4 && timeImage.color != Color.red)
                {
                    timeImage.color = Color.red;

                }

                time.offsetMax = new Vector2(time.offsetMax.x, time.offsetMax.y - 2);
                
            }
            yield return new WaitForSeconds(0.3f);

        }

        Application.LoadLevel(0);
        (cursur.GetComponent("Halo") as Behaviour).enabled = true;

    }
	// Update is called once per frame
	void Update () {
        positionBoat = transform.position;

        position = gameObject.transform.position;
		if (Input.GetKey("left"))
		{
            Left();
            //StartCoroutine(RotateLeft());
            //direction = 2;
            //moving = true;
        }
		if (Input.GetKey("right"))
		   {
            Right();
            //StartCoroutine(RotateRight());
            //direction = 1;
            //moving = true;
        }
		//MoveBoat();
	}
    public void Left()
    {
        position.x -= 1;
        gameObject.transform.position = position;
    }

    public void Right()
    {
        position.x += 1;
        gameObject.transform.position = position;
    }
	void MoveBoat()
	{
		if (moving) {

			switch (direction) {
			case 1:
				position.x += speedx;	
				break;
			case 2:
			position.x -= speedx;
			
				break;
			default:
				moving = false;
				break;
			}
		}

	     position.z += speedz;
		transform.position = position;
	}
	void OnTriggerEnter(Collider other){
        Vector2 past = life.offsetMax;
        life.offsetMax = new Vector2(life.offsetMax.x , life.offsetMax.y - decreaselife);

        Debug.Log(past + " : past | after : "+ life.offsetMax);
        boom.Play ();
		GameObject go = Instantiate (explosionPrefab, Vector3.zero, Quaternion.identity) as GameObject;
		go.transform.position = other.transform.position;
		Destroy (go, 10);
        if (life.offsetMax.y <= -lifecnt.rect.height)
        {
            Application.LoadLevel(0);
            (cursur.GetComponent("Halo") as Behaviour).enabled = true;
        }
           
    }
	private class FlareSystem {
		public string name;
		public GameObject particleObject;
		public ParticleSystem[] particleSystems;
		public bool toggleFlag;
		public bool savedToggleFlag;
	}


}


