﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuToGame : MonoBehaviour {

	public Vector3 Position ;
	public Vector3 Rotation ;
	public Vector3 Scale ;
	public Canvas DetectDisp;
	public Canvas MenuDisp;
	public Canvas Game;
	public Button start;
    //sound
    public AudioSource gameSound;
    //
    public static bool ErrorShowed = false;
    public static bool started = false;
    private static bool move = false;

   


	// Use this for initialization
	void Start () {
        
        start.onClick.AddListener (() => {MoveIt();});
        

	}
	
	// Update is called once per frame
	void Update () {
		if (move && gameObject.transform.position != Position)
			Move ();


	 }

	public static void MoveIt()
	{
        started = true;
		move = true;
        

		//GameDisp.enabled = true;
		//Debug.Log (GameDisp.enabled);
		
        
    }
	private void Move()
	{
        gameSound.Play();
        MenuDisp.enabled = false;
		Game.enabled = true;
        iTween.MoveTo(gameObject, Position,5f);
        StartCoroutine(test());
		iTween.RotateTo(gameObject, Rotation,5f);
	}

    IEnumerator test()
    {
        while (Camera.main.fieldOfView < 60)
        {
            Camera.main.fieldOfView += 1; 
            yield return 0.1f;
        }

    }

}
